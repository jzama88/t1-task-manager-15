package com.t1.alieva.tm.api.controller;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;

public interface IProjectController {

    void createProject() throws AbstractEntityNotFoundException, AbstractFieldException;

    void clearProject();

    void showProjects();

    void showProject(Project project);

    void showProjectById() throws AbstractFieldException;

    void showProjectByIndex() throws AbstractFieldException;

    void updateProjectByIndex() throws AbstractEntityNotFoundException, AbstractFieldException;

    void updateProjectById() throws AbstractEntityNotFoundException, AbstractFieldException;

    void removeProjectById() throws AbstractFieldException;

    void removeProjectByIndex() throws AbstractFieldException;

    void startProjectById() throws AbstractEntityNotFoundException, AbstractFieldException;

    void startProjectByIndex() throws AbstractEntityNotFoundException, AbstractFieldException;

    void completeProjectById() throws AbstractEntityNotFoundException, AbstractFieldException;

    void completeProjectByIndex() throws AbstractEntityNotFoundException, AbstractFieldException;

    void changeProjectStatusById() throws AbstractEntityNotFoundException, AbstractFieldException;

    void changeProjectStatusByIndex() throws AbstractEntityNotFoundException, AbstractFieldException;

}
