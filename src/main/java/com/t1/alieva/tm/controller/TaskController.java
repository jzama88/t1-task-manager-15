package com.t1.alieva.tm.controller;


import com.t1.alieva.tm.api.controller.ITaskController;
import com.t1.alieva.tm.api.service.ITaskService;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() throws AbstractEntityNotFoundException, AbstractFieldException {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = taskService.findAll(sort);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    @Override
    public void showTaskById() throws AbstractFieldException {
        System.out.println(("[SHOW TASK BY ID]"));
        System.out.println(("[ENTER ID]"));
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    @Override
    public void showTaskByIndex() throws AbstractFieldException {
        System.out.println(("[SHOW TASK BY INDEX]"));
        System.out.println(("[ENTER INDEX]"));
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        showTask(task);
    }

    @Override
    public void updateTaskByIndex() throws AbstractEntityNotFoundException, AbstractFieldException {
        System.out.println(("[SHOW TASK BY INDEX]"));
        System.out.println(("[ENTER INDEX]"));
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println(("[ENTER NAME]"));
        final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        final String description = TerminalUtil.nextLine();
        taskService.updateByIndex(index, name, description);
    }

    @Override
    public void updateTaskById() throws AbstractEntityNotFoundException, AbstractFieldException {
        System.out.println(("[UPDATE TASK BY ID]"));
        System.out.println(("[ENTER ID]"));
        final String id = TerminalUtil.nextLine();
        System.out.println(("[ENTER NAME]"));
        final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        final String description = TerminalUtil.nextLine();
        taskService.updateById(id, name, description);
    }

    @Override
    public void removeTaskById() throws AbstractFieldException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
    }

    @Override
    public void removeTaskByIndex() throws AbstractFieldException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(index);
    }

    @Override
    public void startTaskById() throws AbstractFieldException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public void startTaskByIndex() throws AbstractEntityNotFoundException, AbstractFieldException {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public void completeTaskById() throws AbstractFieldException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        taskService.changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public void completeTaskByIndex() throws AbstractEntityNotFoundException, AbstractFieldException {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public void changeTaskStatusById() throws AbstractFieldException {
        System.out.println("[CHANGE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusById(id, status);
    }

    @Override
    public void changeTaskStatusByIndex() throws AbstractEntityNotFoundException, AbstractFieldException {
        System.out.println("[CHANGE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        taskService.changeTaskStatusByIndex(index, status);
    }

    @Override
    public void findTasksByProjectId() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        List<Task> taskList = taskService.findAllByProjectId(projectId);
        for (Task task : taskList) System.out.println(task);
    }

}
