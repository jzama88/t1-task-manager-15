package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.service.ITaskService;
import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.TaskNotFoundException;
import com.t1.alieva.tm.exception.field.*;
import com.t1.alieva.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if(sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if(comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final Task task) throws AbstractEntityNotFoundException {
        if(task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }
    @Override
    public Task create(final String name) throws AbstractFieldException, AbstractEntityNotFoundException {
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        return add(new Task(name));
    }
    @Override
    public Task create(final String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        if(description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return add(new Task(name, description));
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) throws AbstractFieldException{
        if(id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) throws AbstractFieldException{
        if(index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException{
        if(id == null || id.isEmpty()) throw new IdEmptyException();
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        if(task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException{
        if(index == null || index < 0) throw new IndexIncorrectException();
        if(name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if(task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void remove(final Task task) throws AbstractEntityNotFoundException{
        if(task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) throws AbstractFieldException{
        if(id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) throws AbstractFieldException{
        if(index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }
    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException{
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= taskRepository.getSize()) return null;
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) throws AbstractFieldException{
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new IndexIncorrectException();
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId){
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }
}
