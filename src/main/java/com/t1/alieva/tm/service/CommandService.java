package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.api.service.ICommandService;
import com.t1.alieva.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
